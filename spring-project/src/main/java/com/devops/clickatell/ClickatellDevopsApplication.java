package com.devops.clickatell;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ClickatellDevopsApplication {
    public static void main(String[] args) {
        SpringApplication.run(ClickatellDevopsApplication.class, args);
    }

    @GetMapping("/hello")
    public String hello(){
        return "Spring is here!";
    }
}
