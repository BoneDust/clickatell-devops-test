terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.64.2"
    }

    local = {
      source  = "hashicorp/local"
      version = "2.0.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.10.0"
    }
  }

  backend "s3" {
    bucket  = "clickatell-devops-test-tfstate"
    key     = "terraform.tfstate"
    region  = "eu-west-1"
    profile = "goodwill_dev"
  }

  required_version = "> 0.13.1"
}