module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.14.0"
  name    = local.vpc_name
  cidr    = var.vpc_cidr

  azs             = ["${local.region}a", "${local.region}b"]
  private_subnets = var.private_subnet_cidrs
  public_subnets  = var.public_subnet_cidrs

  create_database_subnet_group = false
  manage_default_network_acl   = true
  default_network_acl_tags     = { Name = "${local.resource_prefix}-default" }

  manage_default_route_table = true
  default_route_table_tags   = { Name = "${local.resource_prefix}-default" }

  manage_default_security_group = true
  default_security_group_tags   = { Name = "${local.resource_prefix}-default" }


  enable_nat_gateway = true
  single_nat_gateway = true


  # VPC Flow Logs (Cloudwatch log group and IAM role will be created)
  enable_flow_log                      = true
  create_flow_log_cloudwatch_log_group = true
  create_flow_log_cloudwatch_iam_role  = true
  flow_log_max_aggregation_interval    = 60

  tags = var.project_tags
}