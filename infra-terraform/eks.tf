module "eks" {
  version         = "~> 18.0"
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = local.eks_cluster_name
  cluster_version = var.kubernetes_version
  subnet_ids      = module.vpc.private_subnets
  vpc_id          = module.vpc.vpc_id


  cloudwatch_log_group_retention_in_days = var.log_retention

  # Enable IAM Roles for Service Accounts (IRSA) on the EKS cluster
  enable_irsa = true


  create_cluster_security_group = true
  create_node_security_group    = true
  //might need to check if we need to create additional sg rules to allow nodes to communicate with each other

  cluster_enabled_log_types = [
    "api",
    "audit",
    "authenticator",
    "controllerManager",
    "scheduler"
  ]
  cluster_endpoint_private_access      = var.enable_private_api_server_endpoint
  cluster_endpoint_public_access       = var.enable_public_api_server_endpoint
  cluster_endpoint_public_access_cidrs = var.cluster_endpoint_public_access_cidrs
  cluster_encryption_config            = var.cluster_encryption_config
  cluster_addons = {
    coredns = {
      resolve_conflicts = "OVERWRITE"
      addon_version     = var.addon_coredns_version
    }
    kube-proxy = {
      resolve_conflicts = "OVERWRITE"
      addon_version     = var.addon_kube_proxy_version
    }
    vpc-cni = {
      resolve_conflicts = "OVERWRITE"
      addon_version     = var.addon_vpc_cni_version
    }
  }

  # EKS Managed Node Group(s)
  eks_managed_node_group_defaults = {
    name      = "${local.resource_prefix}-node-group"
    disk_size = 50
    ami_type  = "AL2_x86_64"
    instance_types = [
    "t2.micro"]
  }

  eks_managed_node_groups = {
    blue = {}
    green = {
      min_size     = 1
      max_size     = 2
      desired_size = 2

      instance_types = [
      "t2.micro"]
      capacity_type = "ON_DEMAND"
    }
  }

  # aws-auth configmap
  manage_aws_auth_configmap = true

  aws_auth_users = [
    {
      userarn  = "arn:aws:iam::528377465096:user/goodwill"
      username = "goodwill"
      groups = [
      "system:masters"]
    }
  ]

  aws_auth_accounts = [
    "528377465096"
  ]
}