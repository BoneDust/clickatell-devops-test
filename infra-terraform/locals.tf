locals {
  resource_prefix     = "${local.region_alias}-${var.env}-${var.project_id}"
  iam_resource_prefix = "${var.env}-${var.project_id}"
  region_alias        = lookup(local.region_aliases, var.aws_region)
  region              = var.aws_region

  region_aliases = {
    eu-west-1 = "euw1"
  }

  eks_cluster_name = var.eks_cluster_name == null ? "${local.resource_prefix}-eks" : var.eks_cluster_name
  vpc_name         = "${local.resource_prefix}-vpc"
}
