
/*............. VPC ................*/
output "vpc_id" {
  description = "THe vpc id"
  value       = module.vpc.vpc_id
}

output "vpc_cidr" {
  description = "THe vpc's public subnets"
  value       = module.vpc.vpc_cidr_block
}


/*............ SUBNETS .............*/

output "public_subnets" {
  description = "THe vpc's public subnets"
  value       = module.vpc.public_subnets
}

output "private_subnets" {
  description = "THe vpc's private subnets"
  value       = module.vpc.private_subnets
}

/*............. EKS ................*/

output "cluster_id" {
  description = "The name/id of the EKS cluster. Will block on cluster creation until the cluster is really ready"
  value       = module.eks.cluster_id
}

output "cluster_arn" {
  description = "The Amazon Resource Name (ARN) of the cluster."
  value       = module.eks.cluster_arn
}

output "cluster_certificate_authority_data" {
  description = "Nested attribute containing certificate-authority-data for your cluster. This is the base64 encoded certificate data required to communicate with your cluster."
  value       = module.eks.cluster_certificate_authority_data
  sensitive   = true
}

output "cluster_endpoint" {
  description = "The endpoint for your EKS Kubernetes API."
  value       = module.eks.cluster_endpoint
}

output "cluster_version" {
  description = "The Kubernetes server version for the EKS cluster."
  value       = module.eks.cluster_version
}

output "config_map_aws_auth" {
  description = "A kubernetes configuration to authenticate to this EKS cluster."
  value       = module.eks.aws_auth_configmap_yaml
  sensitive   = true
}
