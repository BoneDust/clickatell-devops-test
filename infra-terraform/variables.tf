
variable "vpc_cidr" {
  description = "VPC cidr range/block"
  type        = string
  default     = "10.0.0.0/26"
}

variable "public_subnet_cidrs" {
  description = "Public subnet cidr range/block"
  type        = list(string)
  default     = ["10.0.0.32/28", "10.0.0.48/28"]
}

variable "private_subnet_cidrs" {
  description = "Private subnet cidr range/block"
  type        = list(string)
  default     = ["10.0.0.0/28", "10.0.0.16/28"]
}

variable "aws_region" {
  description = "AWS Region"
  type        = string
  default     = "eu-west-1"
}

variable "cluster_encryption_config" {
  description = "Configuration block with encryption configuration for the cluster"
  type = list(object({
    provider_key_arn = string
    resources        = list(string)
  }))
  default = []
}

variable "eks_cluster_name" {
  description = "EKS cluster name"
  type        = string
  default     = null
}

variable "subnet_tags" {
  description = "The tag name for the private subnet where EKS resides"
  type        = string
  default     = ""
}

variable "env" {
  description = "Environment Name (dev, sit, uat, prod)"
  type        = string
  default     = "dev"
}

variable "kubernetes_version" {
  type        = string
  description = "Desired Kubernetes master version"
  default     = 1.22
}

variable "log_retention" {
  type        = string
  description = "Number of days to retain logs in Cloudwatch"
  default     = 5
}

variable "project_tags" {
  description = "(Optional) A mapping of tags to assign to each resource"
  type        = map(string)
  default = {
    "terraform" = "yes"
    "creator"   = "Goodwill"
  }
}

variable "project_id" {
  description = "Project Key Id (No spaces allowed)"
  type        = string
  default     = "clickatell"
}

variable "node_group_name" {
  type        = string
  default     = "managed-worker-node-group"
  description = "EKS managed node group name"
}

variable "manage_cluster_iam_resources" {
  type        = bool
  description = "Whether to let the module manage cluster IAM resources. If set to false, cluster_iam_role_name must be specified."
  default     = false
}

variable "manage_worker_iam_resources" {
  type        = bool
  description = "Whether to let the module manage worker IAM resources. If set to false, iam_instance_profile_name must be specified for workers."
  default     = false
}

variable "workers_additional_policies" {
  type        = list(string)
  description = "Additional policies that worker nodes require "
  default = [
    "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore",
  "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"]
}

variable "enable_public_api_server_endpoint" {
  type        = string
  description = "Configure access to the Kubernetes API public server endpoint."
  default     = true
}

variable "enable_private_api_server_endpoint" {
  type        = string
  description = "Configure access to the Kubernetes API private server endpoint."
  default     = false
}

variable "cluster_endpoint_public_access_cidrs" {
  description = "List of CIDR blocks which can access the Amazon EKS public API server endpoint."
  type        = list(string)
  default     = ["0.0.0.0/0"] //Not ideal
}

variable "addon_vpc_cni_version" {
  description = "Version of the VPC CNI to install. If empty you will need to upgrade the CNI yourself during a cluster version upgrade"
  type        = string
  default     = "v1.11.0-eksbuild.1"
}

variable "addon_kube_proxy_version" {
  description = "Version of kube proxy to install. If empty you will need to upgrade kube proxy yourself during a cluster version upgrade"
  type        = string
  default     = "v1.22.6-eksbuild.1"
}

variable "addon_coredns_version" {
  description = "Version of CoreDNS to install. If empty you will need to upgrade CoreDNS yourself during a cluster version upgrade"
  type        = string
  default     = "v1.8.7-eksbuild.1"
}
